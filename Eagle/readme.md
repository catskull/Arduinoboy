# Change log

### 1.1.0
 - Added PS/2 jack for LSDJ keyboard input
 - Removed screw terminals (nobody used them)
 - Replaced MIDI jacks with Jazzmarazz's move versatile footprint
 - Move button and status LED to make room for PS/2 jack

### 1.0.1
 - Fixed GB link port SIN/SOUT pins
 - Fixed routing error causing a bridge between LEDs

### 1.0.0
 - Initial version
 - Added GBC link port for easier use
 - Added screw terminals by request
 - Replaced most components from familiar's design

### 0.0.1
 - Beta/test release
 - Used familiar's design
